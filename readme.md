Block-Overlay File System
=========================

bofs is a [fuse](https://github.com/libfuse/libfuse) based file system that
allows for block based overlays similar to OverlayFS.

Use cases
=========

It might be handy to insulate a block device from potentially destructive
write operations, e.g. a corrupted file system on an external, slow device
that would making a full backup before attempting to rescue the data very
tedious.

Usage
=====

```
# block device with corrupted file system
device="/dev/sdc1"

# directory to store overlayed blocks
mkdir overlay

# mount point
touch mnt

# mount device
bofs -o upper=overlay,lower=$device mnt

# attempt repair
fsck mnt

# changes are stored in overlay directory, lower block device is unchanged
# to undo the changes, simply delete overlay directory contents
rm overlay/*.blk
```

```
# create a 100 MB empty image
bofs -o upper=overlay,lower=/dev/zero,size=$((100*1024*1024)) mnt

# format as fat
mkfs.fat -F 32 mnt

# only six blocks actually stored
ls -1 overlay/
0.blk
1.blk
6.blk
7.blk
32.blk
1608.blk
```

Building the software
=====================

```
meson setup build
meson compile -C build
```

License
=======

Copyright (C) 2024 Tim Wiederhake.

This code is licensed under the terms and conditions of the [GNU General Public
License v2.0 or later](https://spdx.org/licenses/GPL-2.0-or-later.html).

Report bugs and issues [here](https://gitlab.com/twied/bofs/-/issues).
