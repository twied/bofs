// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2024 Tim Wiederhake

#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define FUSE_USE_VERSION 35
#include <fuse.h>
#include <fuse_opt.h>

static int fd_lower;
static int fd_upper;
static off_t backend_size;

static ssize_t min(ssize_t lhs, ssize_t rhs) {
    if (lhs < rhs) {
        return lhs;
    }

    return rhs;
}

static ssize_t file_read(char* buf, size_t size, off_t off) {
    size_t total = 0;
    char filename[BUFSIZ];

    while (total < size) {
        off_t cur_off = off + total;
        size_t cur_size = min(size - total, 512 - (cur_off % 512));

        snprintf(filename, sizeof(filename), "%li.blk", cur_off / 512);

        int fd;
        ssize_t ret;
        if ((fd = openat(fd_upper, filename, O_RDONLY)) >= 0) {
            ret = pread(fd, buf + total, cur_size, cur_off % 512);
            close(fd);
        } else {
            ret = pread(fd_lower, buf + total, cur_size, cur_off);
        }

        if (ret <= 0) {
            return total > 0 ? (ssize_t)total : ret;
        }

        total += ret;
    }

    return total;
}

static ssize_t file_write(const char* buf, size_t size, off_t off) {
    size_t total = 0;
    char filename[BUFSIZ];
    char buffer[512];

    while (total < size) {
        off_t cur_off = off + total;
        size_t cur_size = min(size - total, 512 - (cur_off % 512));
        ssize_t target = file_read(buffer, cur_size, cur_off);

        if (target <= 0) {
            return total > 0 ? (ssize_t)total : target;
        }

        if (memcmp(buffer, buf + total, target) == 0) {
            total += target;
            continue;
        }

        memcpy(buffer, buf + total, target);

        snprintf(filename, sizeof(filename), "%li.blk", cur_off / 512);

        int fd;
        ssize_t ret = -1;
        if ((fd = openat(fd_upper, filename, O_WRONLY | O_CREAT, 0644)) >= 0) {
            ret = pwrite(fd, buffer, target, cur_off % 512);
            fsync(fd);
            close(fd);
        }

        if (ret <= 0) {
            return total > 0 ? (ssize_t)total : ret;
        }

        total += ret;
    }

    return total;
}

static int bofs_open(const char* path, struct fuse_file_info* fi) {
    (void)fi;

    if (strncmp(path, "/", sizeof("/")) != 0) {
        return -ENOENT;
    }

    return 0;
}

static int bofs_read(
        const char* path,
        char* buf,
        size_t size,
        off_t off,
        struct fuse_file_info* fi) {

    (void)fi;

    if (strncmp(path, "/", sizeof("/")) != 0) {
        return -ENOENT;
    }

    off = min(off, backend_size);
    size = min(size, backend_size - off);

    ssize_t ret = file_read(buf, size, off);
    if (ret < 0) {
        return -EIO;
    }

    return ret;
}

static int bofs_write(
        const char* path,
        const char* buf,
        size_t size,
        off_t off,
        struct fuse_file_info* fi) {

    (void)fi;

    if (strncmp(path, "/", sizeof("/")) != 0) {
        return -ENOENT;
    }

    off = min(off, backend_size);
    size = min(size, backend_size - off);

    ssize_t ret = file_write(buf, size, off);
    if (ret < 0) {
        return -EIO;
    }

    return ret;
}

static int bofs_getattr(
        const char* path,
        struct stat* buf,
        struct fuse_file_info* fi) {

    (void)fi;

    if (strncmp(path, "/", sizeof("/")) != 0) {
        return -ENOENT;
    }

    time_t now = time(NULL);

    buf->st_mode = S_IFREG | 0644;
    buf->st_nlink = 1;
    buf->st_uid = getuid();
    buf->st_gid = getgid();
    buf->st_size = backend_size;
    buf->st_blocks = 0;
    buf->st_atime = now;
    buf->st_mtime = now;
    buf->st_ctime = now;

    return 0;
}

struct BofsData {
    const char* path_upper;
    const char* path_lower;
    long unsigned size;
};

const struct fuse_opt options[] = {
    { "upper=%s", offsetof(struct BofsData, path_upper), 1 },
    { "lower=%s", offsetof(struct BofsData, path_lower), 1 },
    { "size=%lu", offsetof(struct BofsData, size), 1 },
    FUSE_OPT_END
};

int main(int argc, char* argv[]) {
    struct BofsData data = { NULL, NULL, 0 };

    struct fuse_args args = FUSE_ARGS_INIT(argc, argv);
    if (fuse_opt_parse(&args, &data, options, NULL) < 0) {
        fprintf(stderr, "error: invalid arguments\n");
        return 1;
    }

    if (data.path_upper == NULL) {
        fprintf(stderr, "error: missing arg: '-o upper=/path/to/directory'\n");
        return 1;
    }

    fd_upper = open(data.path_upper, O_RDONLY | O_DIRECTORY);
    if (fd_upper < 0) {
        fprintf(stderr, "error: cannot open upper directory\n");
        return 1;
    }

    if (data.path_lower == NULL) {
        fprintf(stderr, "error: missing arg: '-o lower=/path/to/device'\n");
        return 1;
    }

    fd_lower = open(data.path_lower, O_RDONLY);
    if (fd_lower < 0) {
        fprintf(stderr, "error: cannot open lower file\n");
        return 1;
    }

    if (data.size != 0) {
        backend_size = data.size;
    } else {
        backend_size = lseek(fd_lower, 0, SEEK_END);
    }

    if (backend_size == (off_t)-1) {
        fprintf(stderr, "error: cannot determine lower file size\n");
        return 1;
    }

    struct fuse_operations ops;
    memset(&ops, '\0', sizeof(struct fuse_operations));
    ops.getattr = bofs_getattr;
    ops.open = bofs_open;
    ops.read = bofs_read;
    ops.write = bofs_write;

    return fuse_main(args.argc, args.argv, &ops, NULL);
}
